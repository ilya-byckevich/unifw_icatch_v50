PHONY += sync release

#some info  final7

sync:
	@echo "Sync all repo"
	@tools/scripts/git_flow.sh sync
	@echo Done.

release:
	@echo "Creating release"
	@tools/scripts/git_flow.sh release
	@echo Done.

hotfix_begin:
	@echo "Creating hotfix"
	@tools/scripts/git_flow.sh hotfix begin 
	@echo Done.

hotfix_end:
	@tools/scripts/git_flow.sh hotfix end 
	@echo Done.

clean:
	@tools/scripts/git_flow.sh clean
	@echo Done.
